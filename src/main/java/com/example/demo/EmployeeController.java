package com.example.demo;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class EmployeeController {

	@Autowired
	private EmployeeRepository repository;
		
	@RequestMapping(method=RequestMethod.GET, 
			        value="/employees",
			        headers="Accept=application/json, application/xml, text/plain")
	public Collection<Employee> getAllEmployees() {
		return repository.getEmployees();
	}

	@RequestMapping(method=RequestMethod.GET, 
			        value="/employees/{id}", 
			        headers="Accept=application/json, application/xml, text/plain")
	public Employee getEmployeeById(@PathVariable long id) {
		return repository.getEmployee(id);
	}
}
